import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClienteModalComponent } from './cliente-modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';


@NgModule({
  declarations: [ClienteModalComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  exports:[ClienteModalComponent,MatDialogModule],
  entryComponents: [
    ClienteModalComponent
  ]
})
export class ClienteModalModule { }
