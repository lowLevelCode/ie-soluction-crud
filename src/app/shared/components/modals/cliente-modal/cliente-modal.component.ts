import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-cliente-modal',
  templateUrl: './cliente-modal.component.html',
  styleUrls: ['./cliente-modal.component.scss']
})
export class ClienteModalComponent implements OnInit {

  form:FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder, 
    private dialogRef: MatDialogRef<ClienteModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      nombre: [this.data.nombre, Validators.required],
      apellidos:[this.data.apellidos, Validators.required],
      telefono:[this.data.telefono, Validators.required],
      telefonoConfirm:['', Validators.required],
      email:[this.data.email, [Validators.required, Validators.email]]
    }); 

    this.dialogRef.disableClose = true;    
  }

  onSubmit(){
    if(this.form.invalid)
      return;    
    this.dialogRef.close(this.form.value);
  }
}
