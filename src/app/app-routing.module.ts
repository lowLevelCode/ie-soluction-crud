import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path:'', redirectTo:'cliente', pathMatch:'full' },
  { path: 'cliente', loadChildren: () => import('./@pages/cliente/cliente.module').then(m => m.ClienteModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
