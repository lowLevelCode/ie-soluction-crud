import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class ClienteService {
    constructor(private readonly http:HttpClient){}

    test():Observable<any>{
        return this.http.get("https://my-json-server.typicode.com/typicode/demo/posts");
    }

    getAllClients():Observable<any>{
        return this.http.get("http://localhost:3000/clientes");
    }

    createClient(cliente:any):Observable<any>{
        return this.http.post("http://localhost:3000/clientes/",cliente);
    }

    updateClient(id:string,cliente:any){
        return this.http.put("http://localhost:3000/clientes/"+id,cliente);
    }

    deleteClient(id:string){
        return this.http.delete("http://localhost:3000/clientes/"+id);
    }
}