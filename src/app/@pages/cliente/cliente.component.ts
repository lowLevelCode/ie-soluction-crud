import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ClienteModalComponent } from 'src/app/shared/components/modals/cliente-modal/cliente-modal.component';
import { ClienteService } from './cliente.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'apellidos', 'telefono','email', 'actions'];
  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  constructor(public dialog: MatDialog, private readonly _clienteService:ClienteService) {}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this._clienteService.test().subscribe(d=>console.log(d));
    this._clienteService.getAllClients().subscribe(d=> this.dataSource = new MatTableDataSource(d));
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  addNew() {    
    const dialogConfig:MatDialogConfig = {
      width:"70%",
      data:{
        btn:{ok:'Guardar'}
      }
    };
    const dialogRef = this.dialog.open(ClienteModalComponent,dialogConfig);

    dialogRef.afterClosed().subscribe(result => { 
      if(!result){        
        return;
      }      
      this._clienteService.createClient(result).subscribe(
        d=>{
          this.dataSource.data.push(result);
          this.dataSource._updateChangeSubscription();  
        },
        err=> console.log("err", err)
      );
      
    });  
  }

  editar(element){
    const {id} = element;    

    const dialogConfig:MatDialogConfig = {
      width:"70%",
      data:{
        ...element,
        btn:{ok:'Actualizar'}
      }
    };
    const dialogRef = this.dialog.open(ClienteModalComponent,dialogConfig);
    dialogRef.afterClosed().subscribe(result => { 
      if(!result){        
        return;
      }      
      this._clienteService.updateClient(id,result).subscribe(
        d=>{
          this.dataSource.data.push(result);
          this.dataSource._updateChangeSubscription();  
        },
        err=> console.log("err", err)
      );
      
    });
  }

  eliminar(element){
    const {id} = element;

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this._clienteService.deleteClient(id).subscribe(
              d=>{
                this._clienteService.getAllClients().subscribe(d=> this.dataSource = new MatTableDataSource(d));
                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                );
              },
              err=> console.log("err", err)
        );        
      }
    })

  }
}
